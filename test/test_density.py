from partialid import DirectedEdge, DAG, valid_dag, parents, create_dag, \
    ConditionalProb, complete_probs, create_cond_probs, discrete_density_dag, \
    observed_density, get_bounds, fix_dag_density
import pandas as pd
import numpy as np
import pytest
import math


@pytest.fixture
def dag():
    vertices = list(range(5))
    edges = {
        DirectedEdge(0, 1),
        DirectedEdge(1, 2),
        DirectedEdge(3, 2),
        DirectedEdge(3, 4),
    }
    return DAG(vertices=vertices, edges=edges)


def test_parents(dag):
    assert parents(2, dag) == {1, 3}


def test_valid_dag(dag):
    assert valid_dag(dag)

    dag.edges.add(DirectedEdge(3, 7))
    assert not valid_dag(dag)


def test_create_dag(dag):
    edges = [(0, 1), (1, 2), (3, 2), (3, 4)]
    assert create_dag(edges) == dag


def test_complete_density_dag():
    probs = [
        ConditionalProb(
            event={'X': 1},
            conditions={},
            mass=.2,
        ),
        ConditionalProb(
            event={'Y': 1},
            conditions={'X': 0},
            mass=.2,
        ),
        ConditionalProb(
            event={'Y': 1},
            conditions={'X': 1},
            mass=.3,
        ),
    ]

    expected = [
        ConditionalProb(
            event={'X': 0},
            conditions={},
            mass=.8,
        ),
        ConditionalProb(
            event={'Y': 0},
            conditions={'X': 0},
            mass=.8,
        ),
        ConditionalProb(
            event={'Y': 0},
            conditions={'X': 1},
            mass=.7,
        ),
    ]

    assert all(p in complete_probs(probs) for p in (probs + expected))


@pytest.fixture
def cond_probs():
    probs = [
        'P(X = 1) = .9',
        'P(Y = 1 | X = 0) = .3',
        'P(Y = 1 | X = 1) = .8',
        'P(Z = 1 | X = 0, Y = 0) = .2',
        'P(Z = 1 | X = 1, Y = 0) = .9',
        'P(Z = 1 | X = 0, Y = 1) = .4',
        'P(Z = 1 | X = 1, Y = 1) = .7',
    ]
    return create_cond_probs(probs)


def test_create_cond_probs(cond_probs):
    assert len(cond_probs) == 14
    z_mass = list(
        filter(lambda p:
               set(p.event.keys()) == {'Z'}
               and set(p.event.values()) == {0}
               and tuple(p.conditions.values()) == (1, 1),
               cond_probs))[0].mass
    assert math.isclose(z_mass, 0.3)


def test_discrete_density_dag(cond_probs):
    dag = create_dag([('X', 'Y'), ('Y', 'Z'), ('X', 'Z')])
    density = discrete_density_dag(dag, cond_probs)
    assert density.density.sum() == 1.0
    assert len(density) == 2**3

    all_zeros = .1 * .7 * .8
    result = density.query('X == 0 and Y == 0 and Z == 0').iloc[0].density
    assert math.isclose(result, all_zeros)


def test_observed_density():
    density = pd.DataFrame({
        'X1': [1, 1, 0, 0],
        'X2': [1, 0, 1, 0],
        'R1': [1, 0, 0, 0],
        'R2': [1, 1, 0, 0],
        'density': [.4, .3, .1, .2]
    })
    observed = observed_density(density)

    expected = pd.DataFrame({
        'X1': [0, 0, 1, 1, '?', '?', '?'],
        'X2': [0, 1, 0, 1, 0, 1, '?'],
        'R1': [1, 1, 1, 1, 0, 0, 0],
        'R2': [1, 1, 1, 1, 1, 1, 0],
        'density': [0, 0, 0, .4, .3, 0, .3],
    })

    exact_cols = ['X1', 'X2', 'R1', 'R2']
    assert np.all(observed[exact_cols] == expected[exact_cols])
    assert np.allclose(observed['density'], expected['density'])


def test_bounds():
    density = pd.DataFrame({
        'X1': [0, 0, 1, 1, '?', '?', '?'],
        'X2': [0, 1, 0, 1, 0, 1, '?'],
        'R1': [1, 1, 1, 1, 0, 0, 0],
        'R2': [1, 1, 1, 1, 1, 1, 0],
        'density': [0, 0, 0, .4, .3, 0, .3],
    })
    assert get_bounds({'X1': 1}, density) == (.4, 1)
    assert get_bounds({'X2': 1}, density) == (.4, .7)
    assert get_bounds({'X1': 1, 'X2': 1}, density) == (.4, .7)


def test_fix():
    dag = create_dag([('X1', 'R2'), ('X2', 'R1')])

    probs = [
        'P(X1 = 1) = .9',
        'P(X2 = 1) = .4',
        'P(R1 = 1 | X2 = 1) = .3',
        'P(R1 = 1 | X2 = 0) = .6',
        'P(R2 = 1 | X1 = 1) = .4',
        'P(R2 = 1 | X1 = 0) = .7',
    ]
    cond_probs = create_cond_probs(probs)
    density = observed_density(discrete_density_dag(dag, cond_probs))
    fixed = fix_dag_density({'R2': 1}, dag, density)
    assert fixed.lowerbound.sum() <= 1
    assert fixed.upperbound.sum() >= 1
