import partialid

# define the ADMG
directed = [
    ('C', 'Z'),
    ('C', 'A'),
    ('C', 'Y'),
    ('Z', 'A'),
    ('A', 'Y'),
]
bidirected = [
    ('A', 'Y'),
]
graph = partialid.create_admg(directed, bidirected)

# calculate bounds
target = partialid.Target(
    outcome_vars=('Y',),
    outcome_vals=(0,),
    treatment_vars=('A',),
    treatment_vals=(0,),
    instrument_vars=('Z',)
)
levels = {'Z': 2, 'A': 2, 'C': 2, 'Y': 2}
bounds = partialid.bounds(graph, target, levels=levels)
print(partialid.pretty_bounds(bounds, levels))
print(partialid.latex_bounds(bounds, levels))

# calculate constraints
constraints = partialid.inequalities(graph, ('Z',), levels)
print(partialid.pretty_inequalities(constraints, levels))
