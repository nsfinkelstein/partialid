import partialid
from partialid import Event, Intervention
from itertools import combinations


def frontdoor_iv():
    directed = [
        ('Z', 'A'),
        ('A', 'M'),
        ('M', 'Y'),
    ]
    bidirected = [
        ('A', 'Y'),
        ('M', 'Y'),
    ]
    iv = partialid.create_admg(directed, bidirected)
    target = partialid.Target(('Y', ), (0, ), ('A', ), (0, ), ('Z', ))
    return iv, target


def backdoor_iv():
    directed = [
        ('C', 'Z'),
        ('C', 'A'),
        ('C', 'Y'),
        ('Z', 'A'),
        ('A', 'Y'),
    ]
    bidirected = [
        ('A', 'Y'),
    ]
    iv = partialid.create_admg(directed, bidirected)
    target = partialid.Target(('Y',), (0,), ('A',), (0,), ('Z',))
    return iv, target


def test_causally_relevant_backdoor():
    admg = backdoor_iv()[0]
    event = partialid.Event(('A', 'Y', 'C'), (0, 0, 0), partialid.Intervention(('Z',), (0,)))
    assert {'A', 'C'} == set(partialid.causally_relevant('Y', event, admg))
    assert {'Z', 'C'} == set(partialid.causally_relevant('A', event, admg))
    assert set() == set(partialid.causally_relevant('C', event, admg))


def test_causally_relevant():
    admg = frontdoor_iv()[0]
    event = partialid.Event(('A', 'Y'), (0, 0), partialid.Intervention(('Z',), (0,)))
    assert 'A' in partialid.causally_relevant('Y', event, admg)


def test_backdoor_contradictions():
    admg = backdoor_iv()[0]
    levels = {v: 2 for v in admg.vertices}

    events = {
        Event(variable=('A', 'Y', 'C'), value=(1, 0, 0), intervention=Intervention(variables=('Z',), values=(0,))),
        Event(variable=('A', 'Y', 'C'), value=(1, 1, 0), intervention=Intervention(variables=('Z',), values=(0,))),
        Event(variable=('A', 'Y', 'C'), value=(0, 0, 1), intervention=Intervention(variables=('Z',), values=(1,))),
        Event(variable=('A', 'Y', 'C'), value=(0, 1, 1), intervention=Intervention(variables=('Z',), values=(1,))),
        Event(variable=('A', 'Y', 'C'), value=(1, 0, 1), intervention=Intervention(variables=('Z',), values=(1,))),
        Event(variable=('A', 'Y', 'C'), value=(1, 1, 1), intervention=Intervention(variables=('Z',), values=(1,))),
    }

    for e1, e2 in combinations(events, r=2):
        assert partialid.contradictory(e1, e2, admg)


def test_summarize_event_sum():
    admg = backdoor_iv()[0]
    levels = {v: 2 for v in admg.vertices}
    events = (
        Event(variable=('Y', 'C', 'A'), value=(0, 0, 1), intervention=Intervention(variables=('Z',), values=(0,))),
        Event(variable=('Y', 'C', 'A'), value=(1, 1, 1), intervention=Intervention(variables=('Z',), values=(0,))),
        Event(variable=('Y', 'C', 'A'), value=(0, 1, 1), intervention=Intervention(variables=('Z',), values=(1,))),
        Event(variable=('Y', 'C', 'A'), value=(1, 0, 1), intervention=Intervention(variables=('Z',), values=(1,))),
    )
    summary = tuple(partialid.summarize_event_sum(events, admg))
    import pprint
    pprint.pprint(summary)
    # print(partialid.pretty_event(summary, levels))
    events = (
        Event(variable=('A', 'C', 'Y'), value=(0, 1, 0), intervention=Intervention(variables=('Z',), values=(0,))),
        Event(variable=('A', 'C', 'Y'), value=(0, 1, 1), intervention=Intervention(variables=('Z',), values=(0,))),
        Event(variable=('A', 'C', 'Y'), value=(1, 0, 1), intervention=Intervention(variables=('Z',), values=(0,))),
        Event(variable=('A', 'C', 'Y'), value=(1, 1, 0), intervention=Intervention(variables=('Z',), values=(0,))),
        Event(variable=('A', 'C', 'Y'), value=(1, 1, 1), intervention=Intervention(variables=('Z',), values=(0,))),
        Event(variable=('A', 'C', 'Y'), value=(1, 0, 0), intervention=Intervention(variables=('Z',), values=(1,))),
    )
    summary = tuple(partialid.summarize_event_sum(events, admg))
    print()
    print()
    import pprint
    pprint.pprint(summary)
