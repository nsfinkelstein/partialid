import partialid
from partialid import Event, Disjunction, Conjunction


def pretty_event(event, levels, indent=0):
    prefix = ' ' * (4 * indent)
    if type(event) == Event:
        return prefix + str(event) + '\n'

    if type(event) in (list, tuple):
        toprint = prefix + 'START EVENT LIST' + '\n'
        for e in event:
            toprint += pretty_event(e, levels, indent + 1)
        toprint += prefix + 'END EVENT LIST' + '\n'
        return toprint

    if type(event) == Disjunction:

        event = partialid.abbreviate_disjunction(event, levels)
        # any conjunction or disjunction with one event is just that event
        if len(event.events) <= 1:
            return pretty_event(event.events[0], levels, indent)

        # otherwise make it look nice
        toprint = prefix + 'START ' + event.__class__.__name__ + '\n'
        for e in event.events:
            toprint += pretty_event(e, levels, indent + 1)
        toprint += prefix + 'END ' + event.__class__.__name__ + '\n'
        return toprint


def pretty_bounds(bounds, levels):
    toprint = pretty_event(bounds[0], levels)

    for Ek in bounds[1:]:
        toprint += 'PLUS \n'
        toprint += 'START MAX \n'
        for gamma_jk in Ek:
            toprint += '    START MAX \n'
            for bound in gamma_jk:
                toprint += '        (\n'
                toprint += pretty_event(bound[0], levels, 3)
                toprint += '            MINUS \n'
                toprint += pretty_event(bound[1], levels, 3)
                toprint += '        )\n'
            toprint += '    END MAX \n'
        toprint += 'END MAX \n'

    return toprint


def latex_event_density(event, levels, indent=0):
    binary = all(v == 2 for v in levels.values())

    prefix = ' ' * (4 * indent)
    if type(event) == Event:
        if binary:
            intervention = ', '.join(
                (r'\bar {}' if v == 0 else '{}').format(k.lower())
                for k, v in zip(event.intervention.variables, event.intervention.values)
            )

            e = ', '.join(
                (r'\bar {}' if v == 0 else '{}').format(k.lower())
                for k, v in zip(event.variable, event.value)
            )

        else:
            intervention = ', '.join(
                '{}_{}'.format(k.lower(), v) for k, v in
                zip(event.intervention.variables, event.intervention.values)
            )

            e = ', '.join(
                '{}_{}'.format(k.lower(), v) for k, v in
                zip(event.variable, event.value)
            )

        density = 'P_{}({})'.format('{' + intervention + '}', e)
        return prefix + density + '\n'

    elif type(event) == Disjunction:
        # any disjunction with one event is just that event
        event = partialid.abbreviate_disjunction(event, levels)
        if len(event.events) == 1:
            return latex_event_density(event.events[0], levels, indent)

        return ' + '.join(latex_event_density(e, levels, indent) for e in event.events)

    else:
        raise RuntimeError('Event is {}'.format(event.__class__))


def latex_bounds(bounds, levels):
    toprint = r'&' + latex_event_density(bounds[0], levels)
    for Ek in bounds[1:]:
        toprint += r'\\ &+ '
        if len(bounds) > 2:
            toprint += r'\max \begin{cases}' + '\n'

        for gamma_jk in Ek:
            toprint += r'    \max \begin{cases} 0\\' + '\n'
            for bound in gamma_jk:
                toprint += latex_event_density(bound[0], levels, 3)
                toprint += '            - '
                bound1 = latex_event_density(bound[1], levels)
                neg_event = r'\big({}\big)' if '+' in bound1 else '{}'
                toprint += neg_event.format(bound1) + r'\\' + '\n'
            toprint += r'    \end{cases} \\' + '\n'

        if len(bounds) > 2:
            toprint += r'\end{cases} \\'
        toprint += r'\\' + '\n'

    return toprint


def latex_event(event, levels, indent=0, toplevel=True):
    binary = all(v == 2 for v in levels.values())

    prefix = ' ' * (4 * indent)
    if type(event) == Event:
        if binary:
            intervention = ', '.join(
                (r'\bar {}' if v == 0 else '{}').format(k.lower())
                for k, v in zip(event.intervention.variables, event.intervention.values)
            )

            e = r' \land '.join(
                (r'\bar {}({})' if v == 0 else '{}({})').format(k.lower(), intervention)
                for k, v in zip(event.variable, event.value)
            )

        else:
            intervention = ', '.join(
                '{}_{}'.format(k.lower(), v) for k, v in
                zip(event.intervention.variables, event.intervention.values)
            )

            e = r' \land '.join(
                '{}_{}({})'.format(k.lower(), v, intervention) for k, v in
                zip(event.variable, event.value)
            )

        if len(event.variable) > 1 and not toplevel:
            e = '(' + e + ')'
        return prefix + e + '\n'

    elif type(event) == Disjunction:
        # any disjunction with one event is just that event
        event = partialid.abbreviate_disjunction(event, levels)
        if len(event.events) == 1:
            return latex_event(event.events[0], levels, indent, False)

        if not toplevel:
            return '(' + r' \lor '.join(latex_event(e, levels, indent, False) for e in event.events) + ')'
        return r' \lor '.join(latex_event(e, levels, indent, False) for e in event.events)

    elif type(event) == Conjunction:
        if not toplevel:
            return '(' + r' \land '.join(latex_event(e, levels, indent, False) for e in event.events) + ')'
        return r' \land '.join(latex_event(e, levels, indent, False) for e in event.events)

    elif type(event) in (list, tuple):
        if len(event) == 1:
            return latex_event(event[0], levels, indent)
        toprint = r'\begin{align*}' + '\n'
        for e in event:
            toprint += latex_event(e, levels) + r'\\' + '\n'
        return toprint + r'\end{align*}'

    else:
        raise RuntimeError('Event is {}'.format(event.__class__))


def latex_bounds_explanation(graph, target, levels):
    partition = partialid.partition(target, levels)
    toprint = r'To begin, we partition $\bar y(\bar a)$ as described in \ref{prop:partition}:' + '\n'
    toprint += r'\begin{align*}'
    toprint += latex_event(partition[0], levels) + r'\\' + '\n'
    toprint += latex_event(partition[1], levels) + r'\\' + '\n'
    toprint += latex_event(partition[2], levels) + r'\\' + '\n'
    toprint += r'\end{align*}' + '\n'
    toprint += 'As before no lower bound is provided for the first event in the partition,\n'
    toprint += 'and the second has an identified density.\n'

    for conjunction in partition[2].events:
        disjunctions, irrelevant = partialid.event_refinement(graph, conjunction, levels)

        toprint += 'We now consider the last event in the partition \n'
        toprint += latex_event(conjunction, levels) + '\n'

        for e1events, e2events, event1, event2 in disjunctions:

            toprint += 'The following events imply '
            toprint += '$' + latex_event(event1, levels) + '$ '
            toprint += r'and can therefore be used as $E_1$ events in \ref{cor:lower-bounds}:' + '\n'
            toprint += latex_event(e1events, levels) + '\n'

            toprint += 'Likewise, the following events imply '
            toprint += '$' + latex_event(event2, levels) + '$ '
            toprint += r'and can therefore be used as $E_2$ events in \ref{cor:lower-bounds}:' + '\n'
            toprint += latex_event(e2events, levels) + '\n'

            toprint += r'By proposition \ref{prop:bound-irrelevance}, $' + latex_event(irrelevant, levels) +  '$,'
            toprint += 'which implies $'+ latex_event(event1, levels) + '$, and therefore \n'
            toprint += 'would be a candidate for use as an $E_1$ event, is redundant.'
            toprint +=  '\n'

            toprint += '\n We now iterate through each potential event for $E_1$'
            toprint += 'and $E_2$, examining the resulting bound. The bound presented'
            toprint += 'above is equal to the density of the identified partition set'
            toprint += ' plus the best of these bouds.'

            for e1_event in e1events:
                toprint += '\n\n'
                toprint += 'The event $' + partialid.latex_event(e1_event, levels) + '$'
                toprint += ' is compatible with \n $'
                toprint += partialid.latex_event(
                    partialid.compatible(e1_event, event2.intervention, graph, levels)
                    , levels)
                toprint += '$. Therefore to compute the bound induced by using it as '
                toprint += 'an $E_1$ event, we must subtract from its density the portion of this compatible event'
                toprint += 'event that does not entail the contradiction of $'
                toprint += partialid.latex_event(event2, levels) + '$. This portion is $'
                neg_term = partialid.negative_term_in_bound(e1_event, event2, graph, levels)
                toprint += partialid.latex_event(neg_term, levels)
                toprint += '$, yielding the bound $'
                toprint += latex_event_density(e1_event, levels)
                toprint += '            - '
                bound1 = latex_event_density(neg_term, levels)
                neg_event = r'\big({}\big)' if '+' in bound1 else '{}'
                toprint += neg_event.format(bound1) + '$.' + '\n\n'



            for e2_event in e2events:
                toprint += '\n\n'
                toprint += 'The event $' + partialid.latex_event(e2_event, levels) + '$'
                toprint += 'is compatible with \n $'
                toprint += partialid.latex_event(
                    partialid.compatible(e2_event, event1.intervention, graph, levels)
                    , levels)
                toprint += '$. Therefore to compute the bound induced by using it as '
                toprint += 'an $E_2$ event, we must subtract from its density the portion of this compatible event'
                toprint += 'event that does not entail the contradiction of $'
                toprint += partialid.latex_event(event1, levels) + '$. This portion is $'
                neg_term = partialid.negative_term_in_bound(e2_event, event1, graph, levels)
                toprint += partialid.latex_event(neg_term, levels)
                toprint += '$, yielding the bound $'
                toprint += latex_event_density(e2_event, levels)
                toprint += '            - '
                bound2 = latex_event_density(neg_term, levels)
                neg_event = r'\big({}\big)' if '+' in bound2 else '{}'
                toprint += neg_event.format(bound2) + '$.' + '\n\n'

    toprint += 'This concludes the derivation of the bounds derived'
    return toprint


def bounds(graph, target, levels):
    partition = partialid.partition(target, levels)
    bounds = [partition[1]]
    for conjunction in partition[2].events:
        disjunctions, irrelevant = partialid.event_refinement(graph, conjunction, levels)

        ek_bounds = set()  # {(0, 0)}
        for e1events, e2events, event1, event2 in disjunctions:
            gamma_jk_bounds = set()
            for e1_event in e1events:
                gamma_jk_bounds.add(
                    (
                        e1_event,
                        partialid.negative_term_in_bound(
                            e1_event, event2, graph, levels
                        )
                    )
                )

            for e2_event in e2events:
                gamma_jk_bounds.add(
                    (
                        e2_event,
                        partialid.negative_term_in_bound(
                            e2_event, event1, graph, levels
                        )
                    )
                )

            ek_bounds.add(tuple(gamma_jk_bounds))

        bounds.append(tuple(ek_bounds))
    return bounds


def pretty_inequalities(ineqs, levels):
    string = ''
    for e, inequality in ineqs.items():
        string += 'bound: ' + str(inequality)
        e = partialid.Disjunction(e)
        string += partialid.pretty_event(e, levels)
    return string


