from collections import namedtuple as nt

Target = nt('Target', 'outcome_vars outcome_vals treatment_vars treatment_vals instrument_vars')
Intervention = nt('Intervention', 'variables values')
Event = nt('Event', 'variable value intervention')
Conjunction = nt('Conjunction', 'events')
Disjunction = nt('Disjunction', 'events')
DirectedEdge = nt('DirectedEdge', 'start end')
BidirectedEdge = nt('BidirectedEdge', 'start end')
ADMG = nt('ADMG', 'vertices directed undirected fixed', defaults=[None])
DAG = nt('Graph', 'vertices edges fixed', defaults=[None])
ConditionalProb = nt('ConditionalProb', 'event conditions mass')
