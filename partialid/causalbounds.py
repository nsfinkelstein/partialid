import partialid
from operator import mul
from functools import reduce
from datetime import datetime
from partialid import DirectedEdge, BidirectedEdge, ADMG, Target, Intervention, Event, Conjunction, Disjunction
from collections import namedtuple as nt
from itertools import product, chain, combinations, combinations_with_replacement


def create_admg(directed, bidirected):
    vertices = list(set(chain.from_iterable(directed + bidirected)))
    directed = {DirectedEdge(start=start, end=end) for start, end in directed}
    bidirected = {BidirectedEdge(start=start, end=end) for start, end in bidirected}
    return ADMG(vertices, directed, bidirected)


def samplespace(variables, levels):
    """ returns outcomes in sample space for discrete variables """
    return list(product(*[range(levels[var]) for var in variables]))


def partition(target, levels):
    # possible interventions
    interventions = [
        Intervention(target.instrument_vars, val) for val in
        samplespace(target.instrument_vars, levels)
    ]

    # no setting of instrument yields treatment (can't be lower bounded)
    event1 = Conjunction(
        # all outcomes take correct value under intervention on treatment
        [
            Event(
                target.outcome_vars,
                target.outcome_vals,
                Intervention(target.treatment_vars, target.treatment_vals),
            )
        ]

        # treatment never takes correct value under intervention on instrument
        + [
            Disjunction([
                Event(
                    target.treatment_vars,
                    treatment_vals,
                    intervention
                )
                for treatment_vals in samplespace(target.treatment_vars, levels)
                if treatment_vals != target.treatment_vals
            ]) for intervention in interventions
        ]
    )

    # first setting of instrument yields correct treatment and outcome
    # outcomes and treatments take values upon intervention on instruments
    event2 = Event(
                 tuple(chain(target.treatment_vars, target.outcome_vars)),
                 tuple(chain(target.treatment_vals, target.outcome_vals)),
                 interventions[0]
             )

    # first setting of instrument yields incorrect values, some setting yields correct
    event3 = Disjunction([
        # event under first intervention yeilds wrong treatment
        Conjunction(
            [
                Event(
                    target.treatment_vars,
                    treatment_vals,
                    interventions[0]
                ),

                Disjunction([
                    Event(
                        tuple(chain(target.treatment_vars, target.outcome_vars)),
                        tuple(chain(target.treatment_vals, target.outcome_vals)),
                        intervention
                    ) for intervention in interventions[1:]
                ])
            ]
        )
        for treatment_vals in samplespace(target.treatment_vars, levels)
        if treatment_vals != target.treatment_vals
    ])

    return event1, event2, event3


def event_refinement(admg, event, levels):
    # returns e1 and e2 events to be used in corollary 3 in the paper

    # the event in world 1 is a conjunction represented by a single event
    event1 = event.events[0]

    # the conjunction of events in the second world
    # i.e. at least one world leads to correct value of treatment and outcome
    event2_disjunction = event.events[1].events

    # e1 events are the observed event with every relevant thing added to it
    # TODO: filter out "other vars" that are not relevant, by appendix A
    # to save on computation
    e1_other_vars = (
        set(admg.vertices) -
        set(event1.variable) -
        set(event1.intervention.variables)
    )
    e1_relevant_vars = chain.from_iterable(
        combinations(e1_other_vars, r=i) for i in range(len(e1_other_vars) + 1)
    )

    e1_events = [
        Event(
            tuple(chain(event1.variable, extra_vars)),
            tuple(chain(event1.value, extra_vals)),
            event1.intervention,
        )
        for extra_vars in e1_relevant_vars
        for extra_vals in samplespace(extra_vars, levels)
    ]

    # filter out irrelevant e1 events. TODO: maybe more can
    # be filtered, as per the proposition in the draft.
    e2_intervention = event2_disjunction[0].intervention

    redundant_e1_events = [
        e for e in e1_events if
        compatible(e, e2_intervention, admg, levels) ==
        space_under_intervention(e2_intervention, admg, levels)
    ]
    e1_events = [e for e in e1_events if e not in redundant_e1_events]

    e2_other_vars = [
        set(admg.vertices) -
        set(event.variable) -
        set(event.intervention.variables)
        for event in event2_disjunction
    ]

    e2_relevant_vars = [
        chain.from_iterable(
            combinations(other_vars, r=i) for i in range(len(other_vars) + 1)
        ) for other_vars in e2_other_vars
    ]

    # one list of events in this list corresponds to
    # one conjunction in the disjunction
    e2_events_list = [[
        Event(
            tuple(chain(event2.variable, extra_vars)),
            tuple(chain(event2.value, extra_vals)),
            event2.intervention,
        )
        for extra_vars in relevant_vars
        for extra_vals in samplespace(extra_vars, levels)
    ] for event2, relevant_vars in zip(event2_disjunction, e2_relevant_vars)]

    bounding_events = [
        (e1_events, e2_events, event1, event2)
        for e2_events, event2 in zip(e2_events_list, event2_disjunction)]

    return bounding_events, redundant_e1_events


def negative_term_in_bound(e_event, other_event, admg, levels):
    compat_e = compatible(e_event, other_event.intervention, admg, levels)
    return Disjunction(tuple([
        event for event in compat_e.events
        if not all(v in event.variable for v in other_event.variable)
        or {
            v: event.value[event.variable.index(v)]
            for v in other_event.variable
        } != dict(zip(other_event.variable, other_event.value))
    ]))


def space_under_intervention(intervention, admg, levels):
    other_variables = set(admg.vertices) - set(intervention.variables)
    return Disjunction([
        Event(tuple(other_variables), vals, intervention)
        for vals in samplespace(other_variables, levels)
    ])


def compatible(event, intervention, admg, levels):
    return Disjunction([
        e for e in space_under_intervention(intervention, admg, levels).events
        if not contradictory(e, event, admg)
    ])


def contradictory(event1, event2, admg):
    # short circuit common use case - same intervention
    if (event1.intervention == event2.intervention
            and event1.variable == event2.variable
            and event1 != event2):
        return True

    # get all shared vars that conflict
    conflict_vars = [
        var for var in set(event1.variable) & set(event2.variable) if
        len(set(e.value[e.variable.index(var)] for e in (event1, event2))) > 1
        and var not in event1.intervention.variables
        and var not in event2.intervention.variables
    ]


    # go through all vars that conflict
    for var in conflict_vars:
        relevant1 = causally_relevant(var, event1, admg)
        relevant2 = causally_relevant(var, event2, admg)

        # any shared relevant vars conflict (condition (i), prop 3)
        shared = set(relevant1) & set(relevant2)
        if not all(relevant1[a] == relevant2[a] for a in shared):
            continue

        noconflict = False
        # condition(ii), prop 3
        e2_vars = (set(event2.variable) | set(event2.intervention.variables))
        diff_vars = set(relevant1) - e2_vars
        for diff_var in diff_vars:
            event2prime = Event(
                tuple(chain(event2.variable, (diff_var,))),
                tuple(chain(event2.value, (relevant1[diff_var] - 1,))),
                event2.intervention,
            )
            if (diff_var in causally_relevant(var, event2prime, admg)
                    and not contradictory(event1, event2prime, admg)):
                noconflict = True
                break

        if noconflict:
            continue

        # condition (iii), prop 3
        e1_vars = (set(event1.variable) | set(event1.intervention.variables))
        diff_vars = set(relevant2) - e1_vars
        for diff_var in diff_vars:
            event1prime = Event(
                tuple(chain(event1.variable, (diff_var,))),
                tuple(chain(event1.value, (relevant2[diff_var] - 2,))),
                event1.intervention,
            )

            if (diff_var in causally_relevant(var, event1prime, admg)
                    and not contradictory(event2, event1prime, admg)):
                noconflict = True
                break

        if noconflict:
            continue

        return True

    return False


def causally_relevant(var, event, admg):
    if var in event.intervention.variables:
        return {}

    # get all vars in event causally relevant to var given the admg
    e = Event(
        tuple(chain(event.variable, event.intervention.variables)),
        tuple(chain(event.value, event.intervention.values)),
        Intervention(tuple(), tuple()),
    )

    relevant = dict()
    next_level = set()
    current_level = (var,)
    while current_level:
        parents = chain.from_iterable(
            partialid.parents(v, admg) for v in current_level
        )

        for var in parents:
            # if parent is in the event, it is relevant
            if var in e.variable:
                relevant[var] = e.value[e.variable.index(var)]

            # otherwise, keep looking up the chain
            else:
                next_level.add(var)

        current_level = next_level
        next_level = set()

    return relevant


def inequalities(admg, instrument_vars, levels):
    interventions = [
        Intervention(instrument_vars, vals)
        for vals in samplespace(instrument_vars, levels)
    ]

    sample_spaces = [
        space_under_intervention(intervention, admg, levels).events
        for intervention in interventions
    ]

    sample_space_combos = [
        list(chain.from_iterable(
            combinations(space, r=i) for i in range(len(space), -1, -1)
        )) for space in sample_spaces
    ]

    ineqs = {}
    compat_cache = {}

    counter = 0
    start = datetime.now()

    # to save time don't try symmetric combinations of events
    space_size = len(sample_space_combos[0])
    num_spaces = len(interventions)
    idx = combinations_with_replacement(range(space_size), num_spaces)
    num_events = reduce(mul, [space_size - i for i in range(num_spaces)])
    for ix in idx:
        events = tuple(chain.from_iterable(
            sample_space_combos[i][j] for i, j in enumerate(ix)))

        counter += 1
        if counter % 10000 == 0:
            print(counter, 'events checked out of', num_events, 'after', datetime.now() - start)

        if not events:
            continue

        comps = build_compat_sets(events, compat_cache, admg)
        bound_value = max(len(x) for x in comps.values())

        toadd = True
        events_set = set(events)
        for e, v in ((set(e), v) for e, v in ineqs.items()):

            # should never hit, given ordering of events
            # if bound_value == v and e.issubset(events_set):
            #     ineqs.pop(e)

            if bound_value == v and events_set.issubset(e):
                toadd = False
                break

        if toadd:
            ineqs[events] = bound_value
            print(bound_value, '>=', partialid.display.latex_event_density(Disjunction(events), levels))

    return ineqs


def summarize_event_sum(events, graph):
    # assume each event specifies a value for each variable
    pieces = group_like_pieces(events, graph)
    updated_pieces = update_pieces(pieces)
    summed_pieces = sum_over_pieces(updated_pieces, graph)
    return summed_pieces

    # TODO: make these comparable (smaller sums, different orders)


def update_pieces(pieces):
    updated_pieces = set()
    for var_combo, events in pieces:
        general_events = tuple(
            Event(
                e.variable,
                tuple(
                    val if var not in var_combo else var.lower()
                    for var, val in zip(e.variable, e.value)
                ),
                e.intervention
            )
            for e in events
        )
        updated_pieces.add((var_combo, general_events))
    return updated_pieces


def sum_over_pieces(pieces, graph):
    var_combos = {variables for variables, events in pieces}

    combos = set()
    # used_levels = {v: 0 for v in graph.vertices}
    restrictions = {}
    for var_combo in var_combos:
        event_sets = tuple(events for vc, events in pieces if vc == var_combo)
        other_vars = tuple(set(graph.vertices) - set(var_combo))
        vals = tuple(
            tuple(
                tuple(
                    e.value[e.variable.index(v)] if v in e.variable else
                    e.intervention.values[e.intervention.variables.index(v)]
                    for v in other_vars)
                for e in events
            ) for events in event_sets)

        for val, events in zip(vals, event_sets):
            # TODO: subsequent maxes cannot use the same values as previous maxes

            # if the max is over the same combination of variables, one of them may
            # take a value previously chosen for that combination, but both cannot

            # if the max is over a single variable that was previously part of a combination
            # that max must now be a different value (otherwise, those events would have been part)
            # of the previous combination

            # TODO: think more about the above, and figure out rules for when
            # the values we MAX over are restricted by previous choices of values
            # we MAXed over, in particular when they cannot be the same
            # (are there other kinds of restrictions?)
            bad_vals = []

            for i in range(len(var_combo), 0, -1):
                for subcombos in combinations(var_combo, r=i):
                    pass


            combos.add((
                    ('max', tuple([
                        '{}={}'.format(v, v.lower()) for v in var_combo
                    ] + [()]
                        # TODO: incorporate restrictions
                    )),
                    ('sum', (
                        ('variables', other_vars),
                        ('over', val),
                    )),
                ))

            restrictions[var_combo] = max_vals
            for var, val in zip(var_combo, max_vals):
                restrictions[var] = val


    return combos


def group_like_pieces(events, graph):
    if not events:
        return set()

    # order variables topologically, so most general stuff comes first
    variables = [
        v for v in partialid.topological_ordering(graph)
        if v in set(events[0].variable) | set(events[0].intervention.variables)
    ]

    for j in range(len(events), 1, -1):
        for event_combo in combinations(events, r=j):
            for i in range(len(variables), 1, -1):
                for var_combo in combinations(variables, r=i):
                    # if all vars are equal, extract this subset, call summarize on the rest
                    vals = {
                        tuple(
                            e.value[e.variable.index(v)] if v in e.variable else
                            e.intervention.values[e.intervention.variables.index(v)]
                            for v in var_combo
                        ) for e in event_combo
                    }

                    if len(vals) == 1:
                        remainder = tuple(set(events) - set(event_combo))
                        return {(var_combo, event_combo)} | group_like_pieces(remainder, graph)

    # no sub-groups, return all events
    return set(events)


def build_compat_sets(events, compat_cache, admg):
    comps = {-1: {events[0]}}
    for i, event in enumerate(events[1:]):
        for j in range(-1, i):
            all_compat = True
            for event2 in comps[j]:
                if (event, event2) in compat_cache:
                    if compat_cache[(event, event2)] is False:
                        all_compat = False
                        break

                elif contradictory(event, event2, admg):
                    compat_cache[(event, event2)] = False
                    all_compat = False
                    break
                else:
                    compat_cache[(event, event2)] = True

            if all_compat:
                comps[j].add(event)

        comps[i] = {event}
    return comps


def abbreviate_disjunction(disjunction, levels):
    if type(disjunction) in (tuple, list):
        disjunction = Disjunction(disjunction)

    # code can't handle nesting yet
    events = disjunction.events
    if any(type(e) == Conjunction for e in events):
        return disjunction

    # TODO: only for same intervention
    if len(set(e.intervention for e in events)) > 1:
        return disjunction

    eliminable = set(chain.from_iterable(e.variable for e in events))

    for var in eliminable:
        other_vars = set(v for e in events for v in e.variable) - {var}
        for i in range(1, len(other_vars) + 1):
            for ov in combinations(other_vars, r=i):
                # events with this subset of variables
                es = {
                    e for e in events if
                    set(e.variable) == (set(ov) | {var})
                }
                sample_space = samplespace(ov, levels)

                for outcome in sample_space:
                    # events with this outcome
                    out_es = {
                        e for e in es
                        if tuple(
                                e.value[e.variable.index(v)] for v in ov
                        ) == outcome
                    }

                    if len(out_es) < levels[var]:
                        continue

                    # every level is represented
                    levels_with_outcome = set(
                        e.value[e.variable.index(var)] for e in out_es
                    )
                    all_levels = set(range(levels[var]))
                    if levels_with_outcome == all_levels:
                        new_es = set(events) - set(out_es)
                        new_es.add(Event(ov, outcome, list(events)[0].intervention))
                        return abbreviate_disjunction(Disjunction(tuple(new_es)), levels)

    return disjunction
