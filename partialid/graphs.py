import numpy as np
import pandas as pd
from itertools import chain, product
from collections import namedtuple as nt
from collections.abc import Iterable

DirectedEdge = nt('DirectedEdge', 'start end')
BidirectedEdge = nt('BidirectedEdge', 'start end')
ADMG = nt('ADMG', 'vertices directed undirected fixed', defaults=[None])
DAG = nt('Graph', 'vertices edges fixed', defaults=[None])
ConditionalProb = nt('ConditionalProb', 'event conditions mass')

# TODO: use something hashable for events instead of dictionaries


def parents(vertex, dag):
    if type(dag) == ADMG:
        dag = DAG(dag.vertices, dag.directed, dag.fixed)
    return {e.start for e in dag.edges if e.end == vertex}


def topological_ordering(dag):
    variables = []
    while set(variables) != set(dag.vertices):
        for v in dag.vertices:
            if v not in set(variables) and set(parents(v, dag)).issubset(set(variables)):
                variables.append(v)

    return variables


def valid_dag(dag):
    edges = set(chain.from_iterable(dag.edges)).issubset(set(dag.vertices))
    return edges and no_cycles(dag)


def no_cycles(dag):
    # TODO
    return True


def create_dag(edges, extra_vertices=None):
    """ edges: list of length-two tuples """
    edges = {DirectedEdge(start=start, end=end) for start, end in edges}
    vertices = list(set(chain.from_iterable(edges))) + (extra_vertices or [])
    dag = DAG(vertices=vertices, edges=edges)

    if not valid_dag(dag):
        raise ValueError('Invalid Dag')

    return dag


# def create_admg(directed, bidirected):
#     vertices = list(set(chain.from_iterable(edges)))
#     edges = {DirectedEdge(start=start, end=end) for start, end in directed}
#     edges = {BidirectedEdge(start=start, end=end) for start, end in bidirected}
#     return ADMG(vertices, directed, bidirected)


def create_cond_probs(probs, levels=None):
    """ probs: list of strings of form 'P(X = 1 | Y = 0, X = 0) = .8' """
    def parse_event(event):
        vertex, val = event.split('=')
        return {vertex.strip(): int(val.strip())}

    def parse_events(events):
        if not events:
            return {}
        if ',' not in events:
            return parse_event(events)
        event = {}
        for e in events.split(','):
            event.update(parse_event(e))
        return event

    def parse_prob(prob):
        cond_bar = prob.find('|')
        close_paren = prob.index(')')
        cond_str = prob[cond_bar + 1:close_paren] if '|' in prob else ''

        event_end = cond_bar if '|' in prob else close_paren
        event_str = prob[prob.index('(') + 1:event_end]
        mass_str = prob[prob.rfind('=') + 1:]

        return ConditionalProb(
            event=parse_event(event_str),
            conditions=parse_events(cond_str),
            mass=float(mass_str.strip()),
        )

    cond_probs = [parse_prob(prob) for prob in probs]
    return complete_probs(cond_probs, levels)


def complete_probs(probs, levels=None):
    """ levels: a dictionary from each vertex to the levels of that vertex
        probs: probabilities for all-but-one level for each random variable
    """
    # TODO: Error handling
    vertices = list(chain.from_iterable(p.event.keys() for p in probs))

    if levels is None:  # default to binary data
        levels = {v: {0, 1} for v in vertices}

    vertex_conditions = {
        v: set(
            tuple(p.conditions.items()) for p in probs
            if set(p.event.keys()) == {v})
        for v in vertices
    }

    vertex_probs = {(v, c): tuple(
        p for p in probs
        if set(p.event.keys()) == {v} and tuple(p.conditions.items()) == c)
                    for v in vertices for c in vertex_conditions[v]}

    complement = [
        ConditionalProb(
            event={
                v:
                list(levels[v] -
                     set(chain.from_iterable(p.event.values()
                                             for p in probs)))[0]
            },
            conditions=dict(c),
            mass=1 - sum(p.mass for p in ps),
        ) for (v, c), ps in vertex_probs.items()
    ]
    return probs + complement


def discrete_density_dag(dag, cond_probs):
    valid_cond_probs_for_dag(dag, cond_probs)
    pa = {v: parents(v, dag) for v in dag.vertices}
    levels = [
        set(
            chain.from_iterable(p.event.values() for p in cond_probs
                                if set(p.event.keys()) == {v}))
        for v in dag.vertices
    ]

    densities = []
    for event in product(*levels):
        vals = dict(zip(dag.vertices, event))
        density = 1
        for v in dag.vertices:
            prob = [
                p for p in cond_probs if set(p.event.keys()) == {v} and all(
                    p.conditions[par] == vals[par]
                    for par in pa[v]) and (vals[v] in p.event.values())
            ]
            assert len(prob) == 1, 'Only one mass must be provided.'
            density *= prob[0].mass

        densities.append({**vals, 'density': density})

    return pd.DataFrame(densities)


def valid_cond_probs_for_dag(dag, cond_probs):
    pa = {v: parents(v, dag) for v in dag.vertices}
    for prob in cond_probs:
        v = list(prob.event.keys())[0]
        par = pa[v]
        cond = set(prob.conditions.keys())
        msg = '{} has parents {}, but conditioned on {}'.format(v, par, cond)
        assert par == cond, msg


def get_bounds(event, density):
    # TODO: handle causal inference, not just missing data
    lb_query = ' and '.join('{} == {}'.format(k, v) for k, v in event.items())
    lcol = 'lowerbound' if 'lowerbound' in density.columns else 'density'
    lower = density.query(lb_query)[lcol].sum()

    ub_query = ' or '.join('({} != {} and {} != "?")'.format(k, v, k)
                           for k, v in event.items())
    ucol = 'upperbound' if 'upperbound' in density.columns else 'density'
    upper = 1 - density.query(ub_query)[ucol].sum()
    # get passed observed density, return bounds on some stuff
    return lower, upper


def fix_dag(vertices, dag):
    if not isinstance(vertices, Iterable):
        vertices = {vertices}

    return DAG(
        edges=[e for e in dag.edges if e.end in vertices],
        vertices=dag.vertices,
        fixed=(dag.fixed or []) + list(vertices),
    )


def fix_dag_density(event, dag, density):
    # TODO: later on, we still assume we have an exact density to start
    # rather than lower and upper bounds
    if 'density' in density.columns:
        density['lowerbound'] = density['upperbound'] = density['density']

    query = ' and '.join('{} == {}'.format(k, v) for k, v in event.items())
    subset = density.query(query).copy()

    cond_probs = dag_factors(dag, density)
    for row in subset.itertuples():
        idx = row[0]
        least_weight = greatest_weight = 1
        for fixed in event:
            vals = {
                parent: row.__getattribute__(parent)
                for parent in parents(fixed, dag)
            }

            # TODO: handle case where multiple parents are unobserved
            if '?' in vals.values():
                all_probs = [p for p in cond_probs if event == p.event]
                print('\n\n',vals, all_probs)
                least_weight *= max(p.mass for p in all_probs)
                greatest_weight *= min(p.mass for p in all_probs)
            else:
                all_probs = [p for p in cond_probs if event == p.event and
                             p.conditions == vals]
                print('\n\n',vals, all_probs)
                assert len(all_probs) == 1, 'Can only have one conditiona prob'
                weight = all_probs[0].mass
                least_weight *= weight
                greatest_weight *= weight

            subset.loc[idx, 'lowerbound'] /= least_weight
            subset.loc[idx, 'upperbound'] /= greatest_weight

    if np.allclose(subset['upperbound'], subset['lowerbound']):
        subset['density'] = subset['upperbound']
        subset = subset.drop('upperbound', axis=1)
        subset = subset.drop('lowerbound', axis=1)
    else:
        subset = subset.drop('density', axis=1)

    return subset


def dag_factors(dag, density):
    # TODO: we still assume we have an exact density, rather than an upper and
    # lower bound on density
    levels = {var: list(set(density[var])) for var in dag.vertices}
    cond_probs = []
    for var in dag.vertices:
        for level in levels[var]:

            if level == '?':  # can't fix to unobserved value
                continue

            pars = parents(var, dag)
            for event in product(*[levels[p] for p in pars]):

                if '?' in event:  # conditioning on indicator == 1
                    continue

                vals = dict(zip(pars, event))
                denom = density.query('{} != "?"'.format(var))
                if len(vals) > 0:
                    items = ('{} == {}'.format(k, v) for k, v in vals.items())
                    denom = denom.query(' and '.join(items))

                num = denom.query('{} == {}'.format(var, level))
                cond_probs.append(
                    ConditionalProb(
                        event={var: level},
                        conditions=vals,
                        mass=num.density.sum() / denom.density.sum()
                    )
                )
    return cond_probs


def observed_density(density):
    # TODO: error handling (bad input, etc)
    indicators = [var for var in density.columns if var[0] == 'R']
    missingness_patterns = density[indicators].drop_duplicates()
    fully_observed = [
        var for var in density.columns
        if var[0] not in ('X', 'R') and var != 'density'
    ]
    partially_observed = [var for var in density.columns if var[0] == 'X']

    levels = {var: set(density[var]) for var in density.columns}

    censor_map = {
        indicator: [
            var for var in density.columns
            if var[0] == 'X' and var[1:] == indicator[1:]
        ][0]
        for indicator in indicators
    }

    densities = []
    for pattern in missingness_patterns.itertuples():
        indications = [(v, pattern.__getattribute__(v)) for v in indicators]
        ind_query = ' and '.join('{} == {}'.format(var, val)
                                 for var, val in indications)

        seen = [censor_map[var] for var, r in indications if r == 1]
        observed = fully_observed + seen
        censored = [censor_map[var] for var, r in indications if r == 0]
        for event in product(*[levels[v] for v in observed]):
            vals = dict(zip(observed, event))
            if len(vals) > 0:
                query = ind_query + ' and ' + ' and '.join(
                    '{} == {}'.format(var, vals[var]) for var in observed)
            else:
                query = ind_query

            densities.append({
                **{var: val
                   for var, val in indications},
                **{var: vals[var]
                   for var in observed},
                **{var: '?'
                   for var in censored},
                'density': density.query(query).density.sum(),
            })

    cols = fully_observed + partially_observed + indicators + ['density']
    return pd.DataFrame(densities)[cols]
