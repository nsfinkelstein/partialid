from setuptools import setup

setup(
    name='partialid',
    version='0.1',
    description='Causal inference and missing data with graphical models',
    url='gitlab.com/nsfinkelstein/partialid',
    packages=['partialid'],
)
