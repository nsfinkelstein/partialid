# Bounds and Inequality Constraints for Generalized Instrumental Variable Graphs.

This library implements the methods described in
[_Deriving Bounds And Inequality Constraints Using Logical Relations Among Counterfactuals_](https://noamfinkelste.in/pdfs/uai2020.pdf).

## Overview

This library can be used for a class of graphs we refer to as _generalized
instrumental variable_ graphs. These are graphs that contain

- An outcome set, denoted **Y**
- A treatment set, denoted **A**
- A generalized instrument set, denoted **Z**

and that satisfy the following two restrictions

- **Z** is causally irrelevant to **Y** given **A**, i.e. there are no directed
  paths from **Z** to **Y** not through **A**
- The joint distribution of **A** and **Y** after intervention on **Z**, denoted
  P(**A**(**z**), **Y**(**z**)), is identified.
  
The library has functions to obtain _bounds_ on the unidentified distribution
P(**Y**(**a**)), and to obtain _inequality constraints_ on the observed data
distribution.

The library can be installed by cloning the repository and then installing locally with pip.
The `-e` option makes the library "editable," so any changes you make to library
code in the cloned directory will be accessible from wherever the library is 
imported. 

```
git clone https://gitlab.com/nsfinkelstein/partialid.git
cd partialid
pip install -e ./
```

## Using the library

The library has utilities for constructing graphs, specifying targets of
inference, and printing bounds and constraints in a readable manner. All the
code in the examples below can be found in a sample script, `sample.py`, in the
root directory of this project.

### Constructing graphs

We begin by constructing the Acyclic Directed Mixed Graph (ADMG) of interest by
listing directed and bidirected edges. In the code below, we construct the
Instrumental Variable model with covariates, described in the paper.


```py
import partialid

directed = [
    ('C', 'Z'),
    ('C', 'A'),
    ('C', 'Y'),
    ('Z', 'A'),
    ('A', 'Y'),
]
bidirected = [
    ('A', 'Y'),
]
graph = partialid.create_admg(directed, bidirected)
```

### Obtaining bounds

To obtain bounds, we must specify the density we would like to bound. In the
example below, we create a target P(Y(A = 0) = 0), and further specify that Z is
the instrument. We use keyword arguments for clarity. 

```py
target = partialid.Target(
    outcome_vars=('Y',), 
    outcome_vals=(0,), 
    treatment_vars=('A',),
    treatment_vals=(0,), 
    instrument_vars=('Z',)
)
```

In this case, the outcome, treatment and instrument vars are all singletons, but
can easily be made into sets by passing longer tuples.

We also specify the cardinality of all the variables in the graph (which are
binary by default).

```py
levels = {'Z': 2, 'A': 2, 'C': 2, 'Y': 2}
```

Bounds can then be obtained with the function call

```py
bounds = partialid.bounds(graph, target, levels=levels)
```
Bounds and inequality constraints are returned in terms of probabilities under
intervention on the instrumental variables, which are identified by assumption.

To print out these bounds nicely, use

```py
print(partialid.pretty_bounds(bounds, levels))
```

To print them out in latex, use

```py
print(partialid.latex_bounds(bounds, levels))
```

### Obtaining inequality constraints

Inequality constraints can be obtained by specifying the graph, the instrumental
variables, and the cardinalities of variables in the graph, as follows

```py
constraints = partialid.inequalities(graph, ('Z',), levels)
```

Generating inequality constraints can be quite time consuming.

To print out inequality constraints, use

```py
print(partialid.pretty_inequalities(constraints, levels))
```

---

Please feel free to get in touch with any questions.
